$(function(){

	// 顶部关注京东显示二维码
	$('#topBar .topBar .f_l ul .icon_follow').hover(function() {
		$(this).children('.wx_code').show();
	}, function() {
		$(this).children('.wx_code').hide();
	});

	// 切换城市
	$('#topBar .topBar .f_l dl dt a').click(function() {
		$('#topBar .topBar .f_l dl dt').addClass('hover');
		$('#topBar .topBar .f_l dl dd').show();
	});
	$('#topBar .topBar .f_l dl').mouseleave(function() {
		$('#topBar .topBar .f_l dl dt').removeClass('hover');
		$('#topBar .topBar .f_l dl dd').hide();
	});

	// vipclub
	$('#topBar .topBar .f_r ul li.vipclub').hover(function() {
		$(this).children('span').addClass('hover');
	}, function() {
		$(this).children('span').removeClass('hover');
	});

	// topBar手机京东
	$('#topBar .topBar .f_r ul li.mobile').hover(function() {
		$(this).find('span.con').addClass('hover');
		$(this).find('.drop_con').show();
	}, function() {
		$(this).find('span.con').removeClass('hover');
		$(this).find('.drop_con').hide();	
	});

	// topBar客户服务
	$('#topBar .topBar .f_r ul li.services').hover(function() {
		$(this).find('span.text').addClass('hover');
		$(this).find('ul').show();
	}, function() {
		$(this).find('span.text').removeClass('hover');
		$(this).find('ul').hide();
	});

	// topBar网站导航
	$('#topBar .topBar .f_r ul li.sitemap').hover(function() {
		$(this).find('span.text').addClass('hover');
		$(this).find('.drop_con').show();
	}, function() {
		$(this).find('span.text').removeClass('hover');
		$(this).find('.drop_con').hide();
	});

	// 顶部图片广告关闭效果
	$('#topAd span').click(function() {
		$('#topAd').hide();
	});

	// 搜索条内文字点击隐藏效果
	// $('#head .search input.text').click(function() {
	// 	$(this).addClass('text2').attr('value','');
	// });

	$('#head .search input.text').focusin(function() {
		$(this).attr('value','').css('color','#333');
	}).focusout(function() {
		$(this).attr('value','剃须刀');
	});

	// 我的京东
	$('#head .myjd').hover(function() {
		$('#head .myjd').addClass('myjd_hover');
		$('#head .userInfo').show();
	}, function() {
		$('#head .myjd').removeClass('myjd_hover');
		$('#head .userInfo').hide();
	});
	$('#head .userInfo').hover(function() {
		$('#head .myjd').addClass('myjd_hover');
		$('#head .userInfo').show();
	}, function() {
		$('#head .myjd').removeClass('myjd_hover');
		$('#head .userInfo').hide();
	});

	// 去购物车结算
	$('#head .shoppingcart').hover(function() {
		$('#head .shoppingcart').addClass('shoppingcart_hover');
		$('#head .allGoods').show();
	}, function() {
		$('#head .shoppingcart').removeClass('shoppingcart_hover');
		$('#head .allGoods').hide();
	});
	$('#head .allGoods').hover(function() {
		$('#head .shoppingcart').addClass('shoppingcart_hover');
		$('#head .allGoods').show();
	}, function() {
		$('#head .shoppingcart').removeClass('shoppingcart_hover');
		$('#head .allGoods').hide();
	});

	// 左侧菜单
	$('#category .category ul li.li_cate').hover(function() {
		var index = $(this).index(),//获取当前li.li_cate的序号
			t_ulcate = $('ul.ul_cate').offset().top,//获取父类容器ul.ul_cate距离页面顶部的距离
			t = $(window).scrollTop(),//获取当前页面内容滚动上去的高度
			t_licate = $(this).offset().top,//获取当前li.li_cate距离页面顶部的距离
			h_subCate = $(this).find('.subCate').height(),//获取当前二级菜单容器.subCate的高度
			h_ulcate = $('ul.ul_cate').height();//获取父类容器ul.ul_cate的高度

		if(h_subCate < h_ulcate){//如果当前二级菜单容器.subCate的高度 小于 父类ul.ul_cate的高度
			if(index == 0){//如果当前li.li_cate是第一个
				$(this).find('.subCate').css('top',t_licate - t_ulcate);//把第一个二级菜单容器.subCate的top值设置为第一个li.li_cate距离顶部的距离 减去 父类容器ul.ul_cate距离页面顶部的距离
			}else{
				$(this).find('.subCate').css('top',(t_licate - t_ulcate) + 1);
			}
		}else{
			if(t > t_ulcate){//如果当前页面内容滚动上去的内容的高度 大于 父类容器ul.ul_cate距离页面顶端的距离（即ul.ul_cate没有被浏览器上边缘遮住的情况）
				if(t_licate - t > 0){//并且当前li.li_cate距离页面顶端的距离 大于 当前页面内容滚动上去的高度
					$(this).find('.subCate').css('top', (t - t_ulcate) + 2);
				}else{
					$(this).find('.subCate').css('top', (t - t_ulcate) - ( - (t_licate - t)) + 2);
				}
			}else{
				$(this).find('.subCate').css('top', '2px' );
			}
		}

		$(this).find('h3').addClass('hover').find('span').hide();
		$(this).find('h3').siblings('.subCate').show();
	}, function() {
		$(this).find('h3').removeClass('hover').find('span').show();
		$(this).find('h3').siblings('.subCate').hide();
	});

	// 第一屏幻灯片切换效果
	// 定义变量c
	var c = 0;
	// 定时器用到的函数
	function run(){
		c++;//让c自增1
		// 当走到最后一张的时候，再从第一张开始
		if(c==6){
			c=0;
		}
		// 当前图片显示，其他图片隐藏
		// 当前圆点变红，其他圆点变灰
		$('.flPicTop .pic li').eq(c).fadeIn(500).siblings('li').hide();
		$('.flPicTop .num li').eq(c).addClass('cur').siblings('li').removeClass('cur');
	}
	// 定义定时器
	var timer = setInterval(run,5000);
	// 给num>li添加鼠标移入事件
	$('.flPicTop .num li').mouseover(function() {
		clearInterval(timer);
		c = $(this).index();		
		// 当前图片显示，其他图片隐藏
		// 当前圆点变红，其他圆点变灰
		$('.flPicTop .pic li').eq(c).fadeIn(500).siblings('li').hide();
		$('.flPicTop .num li').eq(c).addClass('cur').siblings('li').removeClass('cur');
	});
	// 给num>li添加鼠标移出事件
	$('.flPicTop .num li').mouseout(function() {
		timer = setInterval(run,5000);
	});

	// 大轮播图效果下面的 三张图手动切换效果
	var d = 0;
	$('#category .flashPic .flPicBot a.prev').hover(function() {
		$(this).addClass('prev_hover');
	}, function() {
		$(this).removeClass('prev_hover');
	}).click(function() {//鼠标单击事件，单击左边按钮向右滚动
		d++;
		d = (d==1)?-5:d;
		var left = d*609;
		$('#category .flashPic .flPicBot .flPic ul').stop().animate({'left':left+'px'}, 200);
	});
	$('#category .flashPic .flPicBot a.next').hover(function() {
		$(this).addClass('next_hover');
	}, function() {
		$(this).removeClass('next_hover');
	}).click(function() {//鼠标单击事件
		d--;
		d = (d==-6)?0:d;
		var left = d*609;
		$('#category .flashPic .flPicBot .flPic ul').stop().animate({'left':left+'px'}, 200);
	});

	// $('#category .other .life ul.lt > li').mousemove(function() {
	// 	var c = $(this).index();
	// 	$(this).parents('.life_ul').stop().animate({'height':'0px'}, 333);
	// 	$(this).parents('.life_ul').siblings('.life_tab').stop().animate({'height':'167px'}, 333);
	// 	$(this).parents('.life').children('.life_tab').find('li').eq(c).addClass('cur');
	// 	$(this).parents('.life').children('.life_tab').find('.con').eq(c).show();
	// });

	// 解决京东第一屏右侧冲花费鼠标移入显示TAB选项卡，关闭时鼠标无法移出(其实是移出是会触发mousemove事件)的bug（感谢王总，祝你毕业工资15k）
	var cc = 0;
	var xuhao=-1;
	$('#category .other .life .life_ul ul.lt li').mouseenter(function() {
		var xx = $(this).index();
		if(xx !== xuhao && xuhao !== -1){
			$(".tab_arrow ul li").removeClass('cur');
			$(".tab_con").children('.con').hide();
			$(".tab_con").children('.con').eq(xx).hide();
		}
		xuhao = $(this).index();
		if(cc == 0){
			$(this).parents('.life_ul').stop().animate({'height':'0px'}, 300);
			$(this).parents('.life_ul').siblings('.life_tab').stop().animate({'height':'167px'}, 300);
			$(this).parents('.life').children('.life_tab').find('li').eq(xuhao).addClass('cur');
			$(this).parents('.life').children('.life_tab').find('.con').eq(xuhao).show();
			cc++;
		}
		if(xuhao !== 3 && cc !== 0){
			$(this).parents('.life_ul').stop().animate({'height':'0px'}, 300);
			$(this).parents('.life_ul').siblings('.life_tab').stop().animate({'height':'167px'}, 300);
			$(this).parents('.life').children('.life_tab').find('li').eq(xuhao).addClass('cur');
			$(this).parents('.life').children('.life_tab').find('.con').eq(xuhao).show();
		}
	});
	$('#category .other .life').mouseleave(function() {
		cc = 0;
	});

	// 第一屏右侧充话费
	$('#category .other .life .life_tab .tab_arrow ul li').mouseenter(function() {
		var c = $(this).index();
		$(this).addClass('cur').siblings('li').removeClass('cur');
		$(this).parents('.life_tab').find('.con').eq(c).show().siblings('.con').hide();
	});

	$('#category .other .life .life_tab .tab_con .close').click(function() {
		$(this).parents('.life_tab').stop().animate({'height':'0px'}, 333);
		$(this).parents('.life_tab').siblings('.life_ul').stop().animate({'height':'167px'}, 333);
	});

	// lifeWindows
	$('#lifeWindows .items ul li').hover(function() {
		$('#lifeWindows .items ul li .pic').eq($(this).index()).stop().animate({'left':'-10px'}, 333);
	}, function() {
		$('#lifeWindows .items ul li .pic').eq($(this).index()).stop().animate({'left':'0'}, 333);
	});

	// 每层的大tab选项卡切换，基于“当前”元素使用jQuery选择器找对应元素的方法
	$('.floor .commend .tab-arrow ul li').mouseenter(function() {
		// 找到当前tab-arrow>li的序号
		var e = $(this).index();
		// 让c号item显示，其他item隐藏
		$(this).parent('ul').parent('div').siblings('div').children('.item').eq(e).show().siblings('.item').hide();
		// 计算span.cur的left值
		var left = e*158;
		$(this).siblings('.cur').stop().animate({'left':left+'px'}, 100);
		// 判断IE版本（是否IE7）
		if(navigator.userAgent.indexOf("MSIE")>0){
			if(navigator.userAgent.indexOf("MSIE 7.0")>0){
				$(this).parent('ul').children().last().children('span').stop().animate({'left':left+'px'}, 100);
			}
		}
	});

	// 每层楼之间的四张图片切换
	$('.floor .tab-item .item ul .item234 .item234_block li').mouseenter(function(){
		var f = $(this).index();//获取移入的小色块的序号
		var left = f*-473;//计算ul.item234_pic需要移动的距离
		$(this).parent().siblings().stop().animate({'left':left+'px'}, 300);//赋值
		$(this).addClass('cur').siblings('li').removeClass('cur');//对应小色块变色
	});

	// floor03第一个选项卡图片效果
	$('#floor03 .tab-item .item01 ul li a').hover(function() {
		$(this).fadeTo(0,1).siblings('a').fadeTo(0,0.8);
		$(this).parent('li').siblings('li').children('a').fadeTo(0,0.8);
		$(this).find('img').stop().animate({'margin-left':'-15px'}, 500);
	}, function() {
		$(this).siblings('a').fadeTo(0,1);
		$(this).parent('li').parent('ul').find('li').fadeTo(0,1);
		$(this).find('img').stop().animate({'margin-left':'0px'}, 500);
	});

	// floor08图书列表切换
	$('#floor08 .book .tab-arrow ul li').mouseenter(function() {
		var index = $(this).index();
		// 计算span.cur的left值
		var left = index*42;
		$(this).siblings('span').stop().animate({'left':left+'px'}, 300);
		// 对应的bookList显示，其他的隐藏
		$(this).parent().parent().siblings('div').children('.bookList').eq(index).show().siblings('.bookList').hide();
	});

	// 热门晒单
	setInterval(function(){
		$('#sthIndex .share ul li').last().height(0);
		$('#sthIndex .share ul').prepend($('#sthIndex .share ul li').last());
		$('#sthIndex .share ul li').first().stop().animate({'height':'80px'}, 500);
	},6000);

	// 热门活动
	setInterval(function(){
		$('#sthIndex .comment ul li').last().height(0);
		$('#sthIndex .comment ul').prepend($('#sthIndex .comment ul li').last());
		$('#sthIndex .comment ul li').first().stop().animate({'height':'80px'}, 500);
	},6000);
	

	// 对应楼层右侧图标变红
	$('#floorPanel ul li').hover(function() {
		$(this).addClass('hover');
	}, function() {
		$(this).removeClass('hover');
	});
	$(window).scroll(function() {
		var t = $(window).scrollTop();//获取已滚动上去的内容的高度
		var tf1 = $('#floor01').offset().top - t;//计算#floor01到可视区域上边框的距离，以下相同
		var tf2 = $('#floor02').offset().top - t;
		var tf3 = $('#floor03').offset().top - t;
		var tf4 = $('#floor04').offset().top - t;
		var tf5 = $('#floor05').offset().top - t;
		var tf6 = $('#floor06').offset().top - t;
		var tf7 = $('#floor07').offset().top - t;
		var tf8 = $('#floor08').offset().top - t;
		if(t>30){
			$('#floorPanel').fadeIn(500);
		}else{
			$('#floorPanel').fadeOut(500);
		}
		if(tf1<200){
			$('#floorPanel').find('.jdtx').addClass('hover').siblings('li').removeClass('hover');
		}
		if(tf2<200){
			$('#floorPanel').find('.dnsm').addClass('hover').siblings('li').removeClass('hover');
		}
		if(tf3<200){
			$('#floorPanel').find('.fsxb').addClass('hover').siblings('li').removeClass('hover');
		}
		if(tf4<200){
			$('#floorPanel').find('.mrzb').addClass('hover').siblings('li').removeClass('hover');
		}
		if(tf5<200){
			$('#floorPanel').find('.jjsh').addClass('hover').siblings('li').removeClass('hover');
		}
		if(tf6<200){
			$('#floorPanel').find('.mywj').addClass('hover').siblings('li').removeClass('hover');
		}
		if(tf7<200){
			$('#floorPanel').find('.spbj').addClass('hover').siblings('li').removeClass('hover');
		}
		if(tf8<200){
			$('#floorPanel').find('.tsyx').addClass('hover').siblings('li').removeClass('hover');
		}
		if(tf8<-200 || tf1>500){
			$('#floorPanel').find('li').removeClass('hover');
		}
	});

	// 各个楼层点击跳转
	function TzFx(x,y){
		$('#floorPanel ul '+x).click(function() {//1F
			$('body,html').animate({scrollTop:$(y).offset().top - 100}, 500);
			return false;
		});
	}
	TzFx('li.jdtx','#floor01');
	TzFx('li.dnsm','#floor02');
	TzFx('li.fsxb','#floor03');
	TzFx('li.mrzb','#floor04');
	TzFx('li.jjsh','#floor05');
	TzFx('li.mywj','#floor06');
	TzFx('li.spbj','#floor07');
	TzFx('li.tsyx','#floor08');

	// 返回顶部
	$('#floorPanel ul li.fhdb').click(function() {
		$('body,html').animate({scrollTop:0}, 500);
		return false;
	});

	// 当可视区域宽度小于1300px的时候，隐藏floorPanel
	$(window).resize(function() {
		if($(window).width()<1300){
			$('#floorPanel').hide();
		}else{
			$('#floorPanel').show();
		}
	});

});